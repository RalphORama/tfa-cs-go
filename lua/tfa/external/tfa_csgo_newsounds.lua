--DEFAULT
TFA.AddWeaponSound( "TFA_CSGO_Default.Zoom", "weapons/tfa_csgo/zoom.wav")

--GLOBAL
TFA.AddWeaponSound( "csgo_Weapon.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "csgo_Weapon.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "csgo_Weapon.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--AUG
TFA.AddWeaponSound( "TFA_CSGO_AUG.ZoomIn", "weapons/tfa_csgo/aug/aug_zoom_in.wav")
TFA.AddWeaponSound( "TFA_CSGO_AUG.ZoomOut", "weapons/tfa_csgo/aug/aug_zoom_out.wav")
TFA.AddWeaponSound( "TFA_CSGO_AUG.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_AUG.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_AUG.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--AWP
TFA.AddWeaponSound( "TFA_CSGO_AWP.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_AWP.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_AWP.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--AK47
TFA.AddWeaponSound( "TFA_CSGO_AK47.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_AK47.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_AK47.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--Bizon
TFA.AddWeaponSound( "TFA_CSGO_bizon.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_bizon.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_bizon.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--DEagle
TFA.AddWeaponSound( "TFA_CSGO_DEagle.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_DEagle.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_DEagle.WeaponMove3", "weapons/tfa_csgo/movement3.wav")
TFA.AddWeaponSound( "TFA_CSGO_DEagle.WeaponMove4", "weapons/tfa_csgo/deagle/de_draw.wav")

--FAMAS
TFA.AddWeaponSound( "TFA_CSGO_FAMAS.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_FAMAS.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_FAMAS.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--G3SG1
TFA.AddWeaponSound( "TFA_CSGO_G3SG1.Slideback", "weapons/tfa_csgo/g3sg1/g3sg1_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.SlideForward", "weapons/tfa_csgo/g3sg1/g3sg1_slideforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_G3SG1.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_G3SG1.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--GALIL
TFA.AddWeaponSound( "TFA_CSGO_GALIL.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_GALIL.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_GALIL.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--GLOCK18
TFA.AddFireSound( "TFA_CSGO_GLOCK.1", "weapons/tfa_csgo/glock18/glock18-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_GLOCK.Clipin", "weapons/tfa_csgo/glock18/glock_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK.Clipout", "weapons/tfa_csgo/glock18/glock_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK.Slideback", "weapons/tfa_csgo/glock18/glock_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK.Sliderelease", "weapons/tfa_csgo/glock18/glock_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK.Draw", "weapons/tfa_csgo/glock18/glock_draw.wav")

--GRENADES
TFA.AddWeaponSound( "TFA_CSGO_Flashbang.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Flashbang.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Flashbang.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Decoy.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Decoy.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Decoy.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_HEGrenade.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "MOLOTOV.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "INCENDGRENADE.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_HEGrenade.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_HEGrenade.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound ("TFA_CSGO_HEGrenade.Bounce", {"weapons/tfa_csgo/hegrenade/he_bounce-1.wav"} )

TFA.AddWeaponSound( "INCENDGRENADE.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "INCENDGRENADE.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_SmokeGrenade.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_SmokeGrenade.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_SmokeGrenade.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound ("TFA_CSGO_SmokeGrenade.Bounce", {"weapons/tfa_csgo/smokegrenade/grenade_hit1.wav"} )

TFA.AddWeaponSound( "TFA_CSGO_BaseSmokeEffect.Sound", { "weapons/tfa_csgo/smokegrenade/smoke_emit.wav"} )

TFA.AddWeaponSound( "TFA_CSGO_Flashbang.BOOM", { "weapons/tfa_csgo/flashbang/flashbang_explode2.wav", "weapons/tfa_csgo/flashbang/flashbang_explode1.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_BaseGrenade.Explode", { "weapons/tfa_csgo/hegrenade/explode3.wav","weapons/tfa_csgo/hegrenade/explode4.wav", "weapons/tfa_csgo/hegrenade/explode5.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.Loop", { "weapons/tfa_csgo/molotov/fire_loop_1.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.IdleLoop", { "weapons/tfa_csgo/molotov/fire_idle_loop_1.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.FadeOut", { "weapons/tfa_csgo/molotov/fire_loop_fadeout_01.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.Start", { "weapons/tfa_csgo/molotov/molotov_detonate_1.wav", "weapons/tfa_csgo/molotov/molotov_detonate_2.wav", "weapons/tfa_csgo/molotov/molotov_detonate_3.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.Extiguish", { "weapons/tfa_csgo/molotov/molotov_extinguish.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_IncGrenade.Throw", { "weapons/tfa_csgo/incgrenade/inc_grenade_throw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_IncGrenade.Start", { "weapons/tfa_csgo/incgrenade/inc_grenade_detonate_1.wav", "weapons/tfa_csgo/incgrenade/inc_grenade_detonate_2.wav", "weapons/tfa_csgo/incgrenade/inc_grenade_detonate_3.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.Throw", { "weapons/tfa_csgo/molotov/grenade_throw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.IdleLoop", { "weapons/tfa_csgo/molotov/fire_idle_loop_1.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Inferno.IgniteStart", { "weapons/tfa_csgo/molotov/fire_ignite_1.wav","weapons/tfa_csgo/molotov/fire_ignite_2.wav","weapons/tfa_csgo/molotov/fire_ignite_4.wav","weapons/tfa_csgo/molotov/fire_ignite_5.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_SmokeGrenade.Throw", { "weapons/tfa_csgo/smokegrenade/grenade_throw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Flashbang.Throw", { "weapons/tfa_csgo/flashbang/grenade_throw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_HEGrenade.Throw", { "weapons/tfa_csgo/hegrenade/grenade_throw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_Decoy.Throw", { "weapons/tfa_csgo/decoy/grenade_throw.wav" } )

--M4A1
TFA.AddWeaponSound( "TFA_CSGO_M4A1S.BoltBack", "weapons/tfa_csgo/m4a4/m4a4_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1S.BoltForward", "weapons/tfa_csgo/m4a4/m4a4_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.ClipHit", "weapons/tfa_csgo/m4a1/m4a1_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerWeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerWeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerWeaponMove3", "weapons/tfa_csgo/movement3.wav")

--M4A4
TFA.AddWeaponSound( "TFA_CSGO_M4A4.ClipHit", "weapons/tfa_csgo/m4a4/m4a4_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_M4A1.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_M4A1.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--MAC10
TFA.AddWeaponSound( "TFA_CSGO_MAC10.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_MAC10.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_MAC10.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--MP7
TFA.AddWeaponSound( "TFA_CSGO_MP7.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_MP7.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_MP7.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--MP9
TFA.AddWeaponSound( "TFA_CSGO_MP9.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_MP9.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_MP9.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--NOVA
TFA.AddWeaponSound( "TFA_CSGO_NOVA.PumpDuringShot", "weapons/tfa_csgo/nova/nova_pump.wav")
TFA.AddWeaponSound( "TFA_CSGO_NOVA.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_NOVA.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_NOVA.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--P2000
TFA.AddFireSound( "TFA_CSGO_HKP2000.1", "weapons/tfa_csgo/p2000/p2000-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_HKP2000.Clipin", "weapons/tfa_csgo/p2000/p2000_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_HKP2000.Clipout", "weapons/tfa_csgo/p2000/p2000_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_HKP2000.Slideback", "weapons/tfa_csgo/p2000/p2000_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_HKP2000.Sliderelease", "weapons/tfa_csgo/p2000/p2000_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_HKP2000.Draw", "weapons/tfa_csgo/p2000/p2000_draw.wav")

--P90
TFA.AddWeaponSound( "TFA_CSGO_P90.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_P90.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_P90.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--SAWEDOFF
TFA.AddWeaponSound( "TFA_CSGO_SAWEDOFF.PumpDuringShot", "weapons/tfa_csgo/sawedoff/sawedoff_pump.wav")

--SCAR20
TFA.AddWeaponSound( "TFA_CSGO_SCAR20.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_SCAR20.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_SCAR20.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

TFA.AddWeaponSound( "TFA_CSGO_Sawedoff.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_Sawedoff.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_Sawedoff.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--SG556
TFA.AddWeaponSound( "TFA_CSGO_SG556.ZoomIn", "weapons/tfa_csgo/sg556/sg556_zoom_in.wav")
TFA.AddWeaponSound( "TFA_CSGO_SG556.ZoomOut", "weapons/tfa_csgo/sg556/sg556_zoom_out.wav")

TFA.AddWeaponSound( "TFA_CSGO_SG556.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_SG556.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_SG556.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--SSG08
TFA.AddWeaponSound( "TFA_CSGO_SSG08.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_SSG08.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_SSG08.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--TEC 9
TFA.AddWeaponSound( "TFA_CSGO_TEC9.Boltpull", "weapons/tfa_csgo/tec9/tec9_boltpull.wav")

TFA.AddWeaponSound( "TFA_CSGO_TEC9.Boltrelease", "weapons/tfa_csgo/tec9/tec9_boltrelease.wav")

--UMP45
TFA.AddWeaponSound( "TFA_CSGO_UMP45.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_UMP45.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_UMP45.WeaponMove3", "weapons/tfa_csgo/movement3.wav")

--USP-S
TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerWeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerWeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerWeaponMove3", "weapons/tfa_csgo/movement3.wav")

--XM1014
TFA.AddWeaponSound( "TFA_CSGO_XM1014.WeaponMove1", "weapons/tfa_csgo/movement1.wav")
TFA.AddWeaponSound( "TFA_CSGO_XM1014.WeaponMove2", "weapons/tfa_csgo/movement2.wav")
TFA.AddWeaponSound( "TFA_CSGO_XM1014.WeaponMove3", "weapons/tfa_csgo/movement3.wav")