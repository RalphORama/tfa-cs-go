if CLIENT then
      surface.CreateFont( "TFA_CSGO_Nametag", {
      	font = "StratumNo2-Bold",
      	extended = false,
      	size = 25,
      	weight = 500,
      	blursize = 0,
      	scanlines = 0,
      	antialias = true,
      	underline = false,
      	italic = false,
      	strikeout = false,
      	symbol = false,
      	rotary = false,
      	shadow = false,
      	additive = false,
      	outline = false,
      } )
      surface.CreateFont( "TFA_CSGO_Nametag_Small", {
      	font = "StratumNo2-Bold",
      	extended = false,
      	size = 17.5,
      	weight = 500,
      	blursize = 0,
      	scanlines = 0,
      	antialias = true,
      	underline = false,
      	italic = false,
      	strikeout = false,
      	symbol = false,
      	rotary = false,
      	shadow = false,
      	additive = false,
      	outline = false,
      } )
      surface.CreateFont( "TFA_CSGO_NamePicker", {
      	font = "StratumNo2-Bold",
      	extended = false,
      	size = ScreenScale( 6.5 ),
      	weight = 500,
      	blursize = 0,
      	scanlines = 0,
      	antialias = true,
      	underline = false,
      	italic = false,
      	strikeout = false,
      	symbol = false,
      	rotary = false,
      	shadow = false,
      	additive = false,
      	outline = false,
      } )
      hook.Add( "InitPostEntity", "TFA_CSGO_NAMETAG", function()
            if LocalPlayer():GetPData( "tfa_csgo_pdata" ) == nil then
                  LocalPlayer():SetPData( "tfa_csgo_pdata", true )
                  LocalPlayer():SetPData( "tfa_csgo_ak47" .. "_name", "AK-47" )
                  LocalPlayer():SetPData( "tfa_csgo_aug" .. "_name", "AUG" )
                  LocalPlayer():SetPData( "tfa_csgo_awp" .. "_name", "AWP" )
                  LocalPlayer():SetPData( "tfa_csgo_bayonet" .. "_name", "Bayonet" )
                  LocalPlayer():SetPData( "tfa_csgo_bowie" .. "_name", "Bowie Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_butfly" .. "_name", "Butterfly Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_c4" .. "_name", "C4 Explosive" )
                  LocalPlayer():SetPData( "tfa_csgo_cz75" .. "_name", "CZ75-Auto" )
                  LocalPlayer():SetPData( "tfa_csgo_deagle" .. "_name", "Desert Eagle" )
                  LocalPlayer():SetPData( "tfa_csgo_elite" .. "_name", "Dual Berettas" )
                  LocalPlayer():SetPData( "tfa_csgo_falch" .. "_name", "Falchion Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_famas" .. "_name", "FAMAS" )
                  LocalPlayer():SetPData( "tfa_csgo_fiveseven" .. "_name", "Five-SeveN" )
                  LocalPlayer():SetPData( "tfa_csgo_flip" .. "_name", "Flip Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_g3sg1" .. "_name", "G3SG1" )
                  LocalPlayer():SetPData( "tfa_csgo_galil" .. "_name", "Galil AR" )
                  LocalPlayer():SetPData( "tfa_csgo_glock18" .. "_name", "Glock-18" )
                  LocalPlayer():SetPData( "tfa_csgo_gut" .. "_name", "Gut Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_tackni" .. "_name", "Huntsman Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_karam" .. "_name", "Karambit" )
                  LocalPlayer():SetPData( "tfa_csgo_ctknife" .. "_name", "CT Knife" )
				  LocalPlayer():SetPData( "tfa_csgo_knife_classic" .. "_name", "Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_tknife" .. "_name", "T Knife" )
                  LocalPlayer():SetPData( "tfa_csgo_m249" .. "_name", "M249" )
                  LocalPlayer():SetPData( "tfa_csgo_m4a1" .. "_name", "M4A1-S" )
                  LocalPlayer():SetPData( "tfa_csgo_m4a4" .. "_name", "M4A4" )
                  LocalPlayer():SetPData( "tfa_csgo_m9" .. "_name", "M9 Bayonet" )
                  LocalPlayer():SetPData( "tfa_csgo_mac10" .. "_name", "MAC-10" )
                  LocalPlayer():SetPData( "tfa_csgo_mag7" .. "_name", "MAG-7" )
                  LocalPlayer():SetPData( "tfa_csgo_mp5" .. "_name", "MP5" )
                  LocalPlayer():SetPData( "tfa_csgo_mp7" .. "_name", "MP7" )
                  LocalPlayer():SetPData( "tfa_csgo_mp9" .. "_name", "MP9" )
                  LocalPlayer():SetPData( "tfa_csgo_negev" .. "_name", "Negev" )
                  LocalPlayer():SetPData( "tfa_csgo_nova" .. "_name", "Nova" )
                  LocalPlayer():SetPData( "tfa_csgo_p2000" .. "_name", "P2000" )
                  LocalPlayer():SetPData( "tfa_csgo_p250" .. "_name", "P250" )
                  LocalPlayer():SetPData( "tfa_csgo_p90" .. "_name", "P90" )
                  LocalPlayer():SetPData( "tfa_csgo_bizon" .. "_name", "PP Bizon" )
                  LocalPlayer():SetPData( "tfa_csgo_revolver" .. "_name", "R8 Revolver" )
                  LocalPlayer():SetPData( "tfa_csgo_sawedoff" .. "_name", "Sawed Off" )
                  LocalPlayer():SetPData( "tfa_csgo_scar20" .. "_name", "SCAR-20" )
                  LocalPlayer():SetPData( "tfa_csgo_sg556" .. "_name", "SG 553" )
                  LocalPlayer():SetPData( "tfa_csgo_pushkn" .. "_name", "Shadow Daggers" )
                  LocalPlayer():SetPData( "tfa_csgo_ssg08" .. "_name", "SSG 08" )
                  LocalPlayer():SetPData( "tfa_csgo_tec9" .. "_name", "Tec-9" )
                  LocalPlayer():SetPData( "tfa_csgo_ump45" .. "_name", "UMP-45" )
                  LocalPlayer():SetPData( "tfa_csgo_usp" .. "_name", "USP-S" )
                  LocalPlayer():SetPData( "tfa_csgo_xm1014" .. "_name", "XM1014" )
				  
				  --CSGO Classified
				  
				  LocalPlayer():SetPData( "tfa_csgo_galilar" .. "_name", "Galil AR" )
                  LocalPlayer():SetPData( "tfa_csgo_scar17" .. "_name", "SCAR-17" )
				  LocalPlayer():SetPData( "tfa_csgo_knife_classic" .. "_name", "Knife" )
            end
      end)
end