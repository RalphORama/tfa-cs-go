local hudcolor = Color(255, 80, 0, 191)

TFA.AddFireSound( "TFA_CSGO_c4.1", "weapons/tfa_csgo/c4/c4_plant.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_c4.initiate", "weapons/tfa_csgo/c4/c4_initiate.wav")
TFA.AddWeaponSound( "TFA_CSGO_c4.keypressquiet", {"weapons/tfa_csgo/c4/key_press1.wav","weapons/tfa_csgo/c4/key_press2.wav","weapons/tfa_csgo/c4/key_press3.wav","weapons/tfa_csgo/c4/key_press4.wav","weapons/tfa_csgo/c4/key_press5.wav","weapons/tfa_csgo/c4/key_press6.wav","weapons/tfa_csgo/c4/key_press7.wav"})
TFA.AddWeaponSound( "TFA_CSGO_c4.plant", "weapons/tfa_csgo/c4/c4_plant.wav")
TFA.AddWeaponSound( "TFA_CSGO_c4.explode", "weapons/tfa_csgo/c4/c4_explode1.wav")
TFA.AddWeaponSound( "TFA_CSGO_c4.draw", "weapons/tfa_csgo/c4/c4_draw.wav")
sound.Add(
{
	name = "TFA_CSGO_c4.PlantSound",
	channel = CHAN_ITEM,
	level = 60,
	volume = 0.3,
	sound = "weapons/tfa_csgo/c4/c4_beep2.wav"
} )
TFA.AddWeaponSound( "TFA_CSGO_c4.ExplodeWarning", "weapons/tfa_csgo/c4/arm_bomb.wav")
TFA.AddWeaponSound( "TFA_CSGO_c4.ExplodeTriggerTrip", "weapons/tfa_csgo/c4/nvg_on.wav")
TFA.AddWeaponSound( "TFA_CSGO_c4.DefuseStart", "weapons/tfa_csgo/c4/c4_disarmstart.wav")
TFA.AddWeaponSound( "TFA_CSGO_c4.DefuseFinish", "weapons/tfa_csgo/c4/c4_disarmfinish.wav")

if killicon and killicon.Add then
	killicon.Add("tfa_csgo_c4", "vgui/killicons/tfa_csgo_c4", hudcolor)
	killicon.AddAlias("tfa_csgo_c4_ent", "tfa_csgo_c4")
end

if surface then
	surface.CreateFont("TFA_CSGO.C4FontView", {
		font = "Courier New",
		size = 68,
		weight = 600,
	})
	surface.CreateFont("TFA_CSGO.C4FontWorld", {
		font = "Courier New",
		size = 50,
		weight = 600,
	})
end