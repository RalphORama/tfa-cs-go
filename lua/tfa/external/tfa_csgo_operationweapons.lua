TFA.AddAmmo("csgo_sonarbomb", "Tactical Awareness Grenade")
TFA.AddAmmo("csgo_medishot", "Medi-Shot")

TFA.AddFireSound( "TFA_CSGO_HealthShot.Success", "weapons/tfa_csgo/healthshot/healthshot_success_01.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_Sensor.Equip", "weapons/tfa_csgo/sensorgrenade/sensor_equip.wav")

TFA.AddWeaponSound( "TFA_CSGO_Sensor.Activate", {"weapons/tfa_csgo/sensorgrenade/sensor_arm.wav"})

TFA.AddWeaponSound( "TFA_CSGO_Sensor.Land", "weapons/tfa_csgo/sensorgrenade/sensor_land.wav")

TFA.AddWeaponSound( "TFA_CSGO_Sensor.WarmupBeep", "weapons/tfa_csgo/sensorgrenade/sensor_detect.wav")

TFA.AddWeaponSound( "TFA_CSGO_Sensor.DetectPlayer_Hud", "weapons/tfa_csgo/sensorgrenade/sensor_detecthud.wav")

TFA.AddWeaponSound( "TFA_CSGO_Sensor.Detonate", "weapons/tfa_csgo/sensorgrenade/sensor_explode.wav")

TFA.AddWeaponSound( "TFA_CSGO_HealthShot.Pickup", "weapons/tfa_csgo/healthshot/medshot4.wav")

TFA.AddWeaponSound( "TFA_CSGO_HealthShot.Prepare", "weapons/tfa_csgo/healthshot/healthshot_prepare_01.wav")

TFA.AddWeaponSound( "TFA_CSGO_Player.WeaponSelected", "weapons/tfa_csgo/healthshot/wpn_select.wav")

TFA.AddWeaponSound( "TFA_CSGO_IncGrenade.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_IncGrenade.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_IncGrenade.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )