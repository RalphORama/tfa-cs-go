--aK47
TFA.AddFireSound( "TFA_CSGO_AK47.1", "weapons/tfa_csgo/ak47/ak47-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_AK47.Clipout", "weapons/tfa_csgo/ak47/ak47_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_AK47.Clipin", "weapons/tfa_csgo/ak47/ak47_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_AK47.BoltPull", "weapons/tfa_csgo/ak47/ak47_boltpull.wav")

TFA.AddWeaponSound( "TFA_CSGO_AK47.Draw", "weapons/tfa_csgo/ak47/ak47_draw.wav")

--AWP
TFA.AddFireSound( "TFA_CSGO_AWP.1", "weapons/tfa_csgo/awp/awp1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_AWP.Clipout", "weapons/tfa_csgo/awp/awp_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_AWP.Clipin", "weapons/tfa_csgo/awp/awp_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_AWP.ClipHit", "weapons/tfa_csgo/awp/awp_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_AWP.BoltBack", "weapons/tfa_csgo/awp/awp_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_AWP.BoltForward", "weapons/tfa_csgo/awp/awp_boltforward2.wav")

TFA.AddWeaponSound( "TFA_CSGO_AWP.Draw", "weapons/tfa_csgo/awp/awp_draw.wav")

--SG556
TFA.AddFireSound( "TFA_CSGO_SG556.1", "weapons/tfa_csgo/sg556/sg556-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_SG556.Clipout", "weapons/tfa_csgo/sg556/sg556_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_SG556.Clipin", "weapons/tfa_csgo/sg556/sg556_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_SG556.BoltBack", "weapons/tfa_csgo/sg556/sg556_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_SG556.BoltForward", "weapons/tfa_csgo/sg556/sg556_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_SG556.Draw", "weapons/tfa_csgo/sg556/sg556_draw.wav")

--MAC10
TFA.AddFireSound( "TFA_CSGO_MAC10.1", "weapons/tfa_csgo/mac10/mac10-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_MAC10.Clipout", "weapons/tfa_csgo/mac10/mac10_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAC10.Clipin", "weapons/tfa_csgo/mac10/mac10_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAC10.BoltBack", "weapons/tfa_csgo/mac10/mac10_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAC10.BoltForward", "weapons/tfa_csgo/mac10/mac10_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAC10.Draw", "weapons/tfa_csgo/mac10/mac10_draw.wav")

--SCAR20
TFA.AddFireSound( "TFA_CSGO_SCAR20.1", "weapons/tfa_csgo/scar20/scar20_unsil-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_SCAR20.Clipout", "weapons/tfa_csgo/scar20/scar20_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_SCAR20.Clipin", "weapons/tfa_csgo/scar20/scar20_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_SCAR20.BoltBack", "weapons/tfa_csgo/scar20/scar20_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_SCAR20.BoltForward", "weapons/tfa_csgo/scar20/scar20_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_SCAR20.Draw", "weapons/tfa_csgo/scar20/scar20_draw.wav")

--G3SG1
TFA.AddFireSound( "TFA_CSGO_G3SG1.1", "weapons/tfa_csgo/g3sg1/g3sg1-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.Clipout", "weapons/tfa_csgo/g3sg1/g3sg1_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.Clipin", "weapons/tfa_csgo/g3sg1/g3sg1_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.BoltBack", "weapons/tfa_csgo/g3sg1/g3sg1_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.BoltForward", "weapons/tfa_csgo/g3sg1/g3sg1_slideforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_G3SG1.Draw", "weapons/tfa_csgo/g3sg1/g3sg1_draw.wav")

--M4A4
TFA.AddFireSound( "TFA_CSGO_M4A4.1", "weapons/tfa_csgo/m4a4/m4a4-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_M4A4.Clipout", "weapons/tfa_csgo/m4a4/m4a4_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A4.Clipin", "weapons/tfa_csgo/m4a4/m4a4_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A4.BoltBack", "weapons/tfa_csgo/m4a4/m4a4_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A4.BoltForward", "weapons/tfa_csgo/m4a4/m4a4_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A4.Draw", "weapons/tfa_csgo/m4a4/m4a4_draw.wav")

--MP9
TFA.AddFireSound( "TFA_CSGO_MP9.1", "weapons/tfa_csgo/mp9/mp9-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_MP9.Clipout", "weapons/tfa_csgo/mp9/mp9_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP9.Clipin", "weapons/tfa_csgo/mp9/mp9_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP9.BoltBack", "weapons/tfa_csgo/mp9/mp9_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP9.BoltForward", "weapons/tfa_csgo/mp9/mp9_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP9.Draw", "weapons/tfa_csgo/mp9/mp9_draw.wav")

--UMP45
TFA.AddFireSound( "TFA_CSGO_UMP45.1", "weapons/tfa_csgo/ump45/ump45-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_UMP45.Clipout", "weapons/tfa_csgo/ump45/ump45_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_UMP45.Clipin", "weapons/tfa_csgo/ump45/ump45_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_UMP45.BoltBack", "weapons/tfa_csgo/ump45/ump45_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_UMP45.BoltForward", "weapons/tfa_csgo/ump45/ump45_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_UMP45.Draw", "weapons/tfa_csgo/ump45/ump45_draw.wav")

--BIZON
TFA.AddFireSound( "TFA_CSGO_BIZON.1", "weapons/tfa_csgo/bizon/bizon-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_BIZON.Clipout", "weapons/tfa_csgo/bizon/bizon_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_BIZON.Clipin", "weapons/tfa_csgo/bizon/bizon_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_BIZON.BoltBack", "weapons/tfa_csgo/bizon/bizon_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_BIZON.BoltForward", "weapons/tfa_csgo/bizon/bizon_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_BIZON.Draw", "weapons/tfa_csgo/bizon/bizon_draw.wav")

--DEAGLE
TFA.AddFireSound( "TFA_CSGO_DEAGLE.1", "weapons/tfa_csgo/deagle/deagle-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_DEAGLE.Clipout", "weapons/tfa_csgo/deagle/de_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_DEAGLE.Clipin", "weapons/tfa_csgo/deagle/de_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_DEAGLE.Slideback", "weapons/tfa_csgo/deagle/de_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_DEAGLE.Slideforward", "weapons/tfa_csgo/deagle/de_slideforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_DEAGLE.Draw", "weapons/tfa_csgo/deagle/de_draw.wav")

--R8
TFA.AddFireSound( "TFA_CSGO_REVOLVER.1", "weapons/tfa_csgo/revolver/revolver-1_01.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Clipout", "weapons/tfa_csgo/revolver/revolver_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Clipin", "weapons/tfa_csgo/revolver/revolver_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Sideback", "weapons/tfa_csgo/revolver/revolver_sideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Siderelease", "weapons/tfa_csgo/revolver/revolver_siderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Prepare", "weapons/tfa_csgo/revolver/revolver_prepare.wav")

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Hammer", "weapons/tfa_csgo/revolver/revolver_hammer.wav")

TFA.AddWeaponSound( "TFA_CSGO_REVOLVER.Draw", "weapons/tfa_csgo/revolver/revolver_draw.wav")

--P90
TFA.AddFireSound( "TFA_CSGO_P90.1", "weapons/tfa_csgo/p90/p90-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_P90.Clipout", "weapons/tfa_csgo/p90/p90_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_P90.Clipin", "weapons/tfa_csgo/p90/p90_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_P90.Cliprelease", "weapons/tfa_csgo/p90/p90_cliprelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_P90.ClipHit", "weapons/tfa_csgo/p90/p90_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_P90.BoltBack", "weapons/tfa_csgo/p90/p90_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_P90.BoltForward", "weapons/tfa_csgo/p90/p90_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_P90.Draw", "weapons/tfa_csgo/p90/p90_draw.wav")

--GALIL
TFA.AddFireSound( "TFA_CSGO_GALIL.1", "weapons/tfa_csgo/galil/galil-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_GALIL.Clipout", "weapons/tfa_csgo/galil/galil_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_GALIL.Clipin", "weapons/tfa_csgo/galil/galil_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_GALIL.BoltBack", "weapons/tfa_csgo/galil/galil_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_GALIL.BoltForward", "weapons/tfa_csgo/galil/galil_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_GALIL.Draw", "weapons/tfa_csgo/galil/galil_draw.wav")

--Aug
TFA.AddFireSound( "TFA_CSGO_AUG.1", "weapons/tfa_csgo/aug/aug-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_AUG.Clipout", "weapons/tfa_csgo/aug/aug_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_AUG.Clipin", "weapons/tfa_csgo/aug/aug_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_AUG.Cliphit", "weapons/tfa_csgo/aug/aug_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_AUG.Boltpull", "weapons/tfa_csgo/aug/aug_boltpull.wav")

TFA.AddWeaponSound( "TFA_CSGO_AUG.Boltrelease", "weapons/tfa_csgo/aug/aug_boltrelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_AUG.Draw", "weapons/tfa_csgo/aug/aug_draw.wav")

--Famas
TFA.AddFireSound( "TFA_CSGO_FAMAS.1", "weapons/tfa_csgo/famas/famas-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_FAMAS.Clipout", "weapons/tfa_csgo/famas/famas_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_FAMAS.Clipin", "weapons/tfa_csgo/famas/famas_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_FAMAS.Cliphit", "weapons/tfa_csgo/famas/famas_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_FAMAS.BoltBack", "weapons/tfa_csgo/famas/famas_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_FAMAS.BoltForward", "weapons/tfa_csgo/famas/famas_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_FAMAS.Draw", "weapons/tfa_csgo/famas/famas_draw.wav")

--M4A1
TFA.AddFireSound( "TFA_CSGO_M4A1.2", "weapons/tfa_csgo/m4a1/m4a1-1-unsil.wav", true )

TFA.AddFireSound( "TFA_CSGO_M4A1.1", "weapons/tfa_csgo/m4a1/m4a1-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_M4A1.Clipout", "weapons/tfa_csgo/m4a4/m4a4_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.Clipin", "weapons/tfa_csgo/m4a4/m4a4_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.BoltBack", "weapons/tfa_csgo/m4a4/m4a4_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.BoltForward", "weapons/tfa_csgo/m4a4/m4a4_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.Draw", "weapons/tfa_csgo/m4a4/m4a4_draw.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrew1", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_1.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrew2", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_2.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrew3", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_3.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrew4", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_4.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrew5", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_5.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrewOnStart", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_on_start.wav")

TFA.AddWeaponSound( "TFA_CSGO_M4A1.SilencerScrewOffEnd", "weapons/tfa_csgo/m4a1/m4a1_silencer_screw_off_end.wav")

--Scout
TFA.AddFireSound( "TFA_CSGO_SSG08.1", "weapons/tfa_csgo/ssg08/ssg08-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_SSG08.Clipout", "weapons/tfa_csgo/ssg08/ssg08_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_SSG08.Clipin", "weapons/tfa_csgo/ssg08/ssg08_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_SSG08.ClipHit", "weapons/tfa_csgo/ssg08/ssg08_cliphit.wav")

TFA.AddWeaponSound( "TFA_CSGO_SSG08.BoltBack", "weapons/tfa_csgo/ssg08/ssg08_boltback.wav")

TFA.AddWeaponSound( "TFA_CSGO_SSG08.BoltForward", "weapons/tfa_csgo/ssg08/ssg08_boltforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_SSG08.Draw", "weapons/tfa_csgo/ssg08/ssg08_draw.wav")

--Neggy
TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Draw", "weapons/tfa_csgo/mag7/mag7_draw.wav")

TFA.AddFireSound( "TFA_CSGO_NEGEV.1", "weapons/tfa_csgo/negev/negev-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Boxout", "weapons/tfa_csgo/negev/negev_boxout.wav")

TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Boxin", "weapons/tfa_csgo/negev/negev_boxin.wav")

TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Chain", "weapons/tfa_csgo/negev/negev_chain.wav")

TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Coverdown", "weapons/tfa_csgo/negev/negev_coverdown.wav")

TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Coverup", "weapons/tfa_csgo/negev/negev_coverup.wav")

TFA.AddWeaponSound( "TFA_CSGO_NEGEV.Pump", "weapons/tfa_csgo/negev/negev_pump.wav")

sound.Add({
	name = "TFA_CSGO_NEGEV.Clean1",
	channel = CHAN_WEAPON,
	volume = 1.0,
	sound = "weapons/tfa_csgo/negev/negev_clean_01.wav",
	pitch = {97, 100}
})

sound.Add({
	name = "TFA_CSGO_NEGEV.Clean2",
	channel = CHAN_WEAPON,
	volume = 1.0,
	sound = "weapons/tfa_csgo/negev/negev_clean_02.wav",
	pitch = {100, 103}
})

--M249
TFA.AddWeaponSound( "TFA_CSGO_M249.Draw", "weapons/tfa_csgo/mag7/mag7_draw.wav")

TFA.AddFireSound( "TFA_CSGO_M249.1", "weapons/tfa_csgo/m249/m249-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_M249.Boxout", "weapons/tfa_csgo/m249/m249_boxout.wav")

TFA.AddWeaponSound( "TFA_CSGO_M249.Boxin", "weapons/tfa_csgo/m249/m249_boxin.wav")

TFA.AddWeaponSound( "TFA_CSGO_M249.Chain", "weapons/tfa_csgo/m249/m249_chain.wav")

TFA.AddWeaponSound( "TFA_CSGO_M249.Coverdown", "weapons/tfa_csgo/m249/m249_coverdown.wav")

TFA.AddWeaponSound( "TFA_CSGO_M249.Coverup", "weapons/tfa_csgo/m249/m249_coverup.wav")

TFA.AddWeaponSound( "TFA_CSGO_M249.Pump", "weapons/tfa_csgo/m249/m249_pump.wav")

--NOVA
TFA.AddFireSound( "TFA_CSGO_NOVA.1", "weapons/tfa_csgo/nova/nova-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_NOVA.Insertshell", "weapons/tfa_csgo/nova/nova_insertshell.wav")

TFA.AddWeaponSound( "TFA_CSGO_NOVA.Pump", "weapons/tfa_csgo/nova/nova_pump.wav")

TFA.AddWeaponSound( "TFA_CSGO_NOVA.Draw", "weapons/tfa_csgo/nova/nova_draw.wav")

--MAG7
TFA.AddFireSound( "TFA_CSGO_MAG7.1", "weapons/tfa_csgo/mag7/mag7-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_MAG7.Clipout", "weapons/tfa_csgo/mag7/mag7_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAG7.Clipin", "weapons/tfa_csgo/mag7/mag7_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAG7.PumpBack", "weapons/tfa_csgo/mag7/mag7_pump_back.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAG7.PumpForward", "weapons/tfa_csgo/mag7/mag7_pump_forward.wav")

TFA.AddWeaponSound( "TFA_CSGO_MAG7.Draw", "weapons/tfa_csgo/mag7/mag7_draw.wav")

--XM1014
TFA.AddFireSound( "TFA_CSGO_XM1014.1", "weapons/tfa_csgo/xm1014/xm1014-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_XM1014.Insertshell", "weapons/tfa_csgo/xm1014/xm1014_insertshell.wav")

TFA.AddWeaponSound( "TFA_CSGO_XM1014.Draw", "weapons/tfa_csgo/xm1014/xm1014_draw.wav")

--SAWEDOFF
TFA.AddFireSound( "TFA_CSGO_SAWEDOFF.1", "weapons/tfa_csgo/sawedoff/sawedoff-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_SAWEDOFF.Insertshell", "weapons/tfa_csgo/sawedoff/sawedoff_insertshell.wav")

TFA.AddWeaponSound( "TFA_CSGO_SAWEDOFF.Pump", "weapons/tfa_csgo/sawedoff/sawedoff_pump.wav")

TFA.AddWeaponSound( "TFA_CSGO_SAWEDOFF.Draw", "weapons/tfa_csgo/sawedoff/sawedoff_draw.wav")

--USP
TFA.AddFireSound( "TFA_CSGO_USP.2", "weapons/tfa_csgo/usp/usp_unsil-1.wav", true )

TFA.AddFireSound( "TFA_CSGO_USP.1", "weapons/tfa_csgo/usp/usp1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_USP.Clipout", "weapons/tfa_csgo/usp/usp_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.Clipin", "weapons/tfa_csgo/usp/usp_clipin_point20.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.BoltBack", "weapons/tfa_csgo/usp/usp_slideback2.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.BoltForward", "weapons/tfa_csgo/usp/usp_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.Draw", "weapons/tfa_csgo/usp/usp_draw.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrew1", "weapons/tfa_csgo/usp/usp_silencer_screw1.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrew2", "weapons/tfa_csgo/usp/usp_silencer_screw2.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrew3", "weapons/tfa_csgo/usp/usp_silencer_screw3.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrew4", "weapons/tfa_csgo/usp/usp_silencer_screw4.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrew5", "weapons/tfa_csgo/usp/usp_silencer_screw5.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrewOnStart", "weapons/tfa_csgo/usp/usp_silencer_screw_on_start.wav")

TFA.AddWeaponSound( "TFA_CSGO_USP.SilencerScrewOffEnd", "weapons/tfa_csgo/usp/usp_silencer_screw_off_end.wav")

--CZ75
TFA.AddFireSound( "TFA_CSGO_CZ75.1", "weapons/tfa_csgo/cz75a/cz75a-1.wav", true )

--P250
TFA.AddFireSound( "TFA_CSGO_P250.1", "weapons/tfa_csgo/p250/p250-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_P250.Clipin", "weapons/tfa_csgo/p250/p250_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_P250.Clipout", "weapons/tfa_csgo/p250/p250_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_P250.Slideback", "weapons/tfa_csgo/p250/p250_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_P250.Sliderelease", "weapons/tfa_csgo/p250/p250_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_P250.Draw", "weapons/tfa_csgo/p250/p250_draw.wav")

--GLOCK18
TFA.AddFireSound( "TFA_CSGO_GLOCK18.1", "weapons/tfa_csgo/glock18/glock18-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_GLOCK18.Clipin", "weapons/tfa_csgo/glock18/glock_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK18.Clipout", "weapons/tfa_csgo/glock18/glock_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK18.Slideback", "weapons/tfa_csgo/glock18/glock_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK18.Sliderelease", "weapons/tfa_csgo/glock18/glock_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_GLOCK18.Draw", "weapons/tfa_csgo/glock18/glock_draw.wav")

--P2000
TFA.AddFireSound( "TFA_CSGO_P2000.1", "weapons/tfa_csgo/p2000/p2000-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_P2000.Clipin", "weapons/tfa_csgo/p2000/p2000_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_P2000.Clipout", "weapons/tfa_csgo/p2000/p2000_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_P2000.Slideback", "weapons/tfa_csgo/p2000/p2000_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_P2000.Sliderelease", "weapons/tfa_csgo/p2000/p2000_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_P2000.Draw", "weapons/tfa_csgo/p2000/p2000_draw.wav")

--ELITE
TFA.AddFireSound( "TFA_CSGO_ELITE.1", "weapons/tfa_csgo/elite/elite-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_ELITE.Lclipin", "weapons/tfa_csgo/elite/elite_leftclipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_ELITE.Rclipin", "weapons/tfa_csgo/elite/elite_rightclipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_ELITE.Clipout", "weapons/tfa_csgo/elite/elite_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_ELITE.Sliderelease", "weapons/tfa_csgo/elite/elite_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_ELITE.Draw", "weapons/tfa_csgo/elite/elite_draw.wav")

sound.Add(
{
	name = "TFA_CSGO_ELITE.TauntLook1",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.1,
	pitch = 90,
	sound = "weapons/tfa_csgo/elite/elite_draw.wav"
} )

sound.Add(
{
	name = "TFA_CSGO_ELITE.TauntLook2",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.05,
	pitch = 90,
	sound = "weapons/tfa_csgo/elite/elite_draw.wav"
} )

sound.Add(
{
	name = "TFA_CSGO_ELITE.TauntStartTap",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.1,
	pitch = 90,
	sound = "weapons/tfa_csgo/elite/elite_draw.wav"
} )

sound.Add(
{
	name = "TFA_CSGO_ELITE.TauntTap1",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.25,
	pitch = 90,
	sound = "weapons/tfa_csgo/elite/elite_taunt_tap.wav"
} )

sound.Add(
{
	name = "TFA_CSGO_ELITE.TauntTap2",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.25,
	pitch = 90,
	sound = "weapons/tfa_csgo/elite/elite_taunt_tap.wav"
} )

sound.Add(
{
	name = "TFA_CSGO_ELITE.TauntTwirl",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.15,
	pitch = 95,
	sound = "weapons/tfa_csgo/elite/elite_draw.wav"
} )

--TEC9
TFA.AddFireSound( "TFA_CSGO_TEC9.1", "weapons/tfa_csgo/tec9/tec9-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_TEC9.Clipin", "weapons/tfa_csgo/tec9/tec9_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_TEC9.Clipout", "weapons/tfa_csgo/tec9/tec9_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_TEC9.Slideback", "weapons/tfa_csgo/tec9/tec9_boltpull.wav")

TFA.AddWeaponSound( "TFA_CSGO_TEC9.Sliderelease", "weapons/tfa_csgo/tec9/tec9_boltrelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_TEC9.Draw", "weapons/tfa_csgo/tec9/tec9_draw.wav")

--MP7
TFA.AddFireSound( "TFA_CSGO_MP7.1", "weapons/tfa_csgo/mp7/mp7-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_MP7.Clipout", "weapons/tfa_csgo/mp7/mp7_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP7.Clipin", "weapons/tfa_csgo/mp7/mp7_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP7.Slideback", "weapons/tfa_csgo/mp7/mp7_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP7.SlideForward", "weapons/tfa_csgo/mp7/mp7_slideforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP7.Draw", "weapons/tfa_csgo/mp7/mp7_draw.wav")

--FIVESEVEN
TFA.AddFireSound( "TFA_CSGO_FIVESEVEN.1", "weapons/tfa_csgo/fiveseven/fiveseven-1.wav", true )

TFA.AddWeaponSound( "TFA_CSGO_FIVESEVEN.Clipout", "weapons/tfa_csgo/fiveseven/fiveseven_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_FIVESEVEN.Clipin", "weapons/tfa_csgo/fiveseven/fiveseven_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_FIVESEVEN.Slideback", "weapons/tfa_csgo/fiveseven/fiveseven_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_FIVESEVEN.Sliderelease", "weapons/tfa_csgo/fiveseven/fiveseven_sliderelease.wav")

TFA.AddWeaponSound( "TFA_CSGO_FIVESEVEN.Draw", "weapons/tfa_csgo/fiveseven/fiveseven_draw.wav")

--MP5
TFA.AddFireSound( "TFA_CSGO_MP5.1", { "weapons/tfa_csgo/mp5/mp5-1.wav" } , true )

TFA.AddWeaponSound( "TFA_CSGO_MP5.Clipout", "weapons/tfa_csgo/mp5/mp5_clipout.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP5.Clipin", "weapons/tfa_csgo/mp5/mp5_clipin.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP5.Slideback", "weapons/tfa_csgo/mp5/mp5_slideback.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP5.SlideForward", "weapons/tfa_csgo/mp5/mp5_slideforward.wav")

TFA.AddWeaponSound( "TFA_CSGO_MP5.Draw", "weapons/tfa_csgo/mp5/mp5_draw.wav")

--Flashbang
TFA.AddWeaponSound( "TFA_CSGO_FLASHGRENADE.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_FLASHGRENADE.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_FLASHGRENADE.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_FRAGGRENADE.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_MOLOTOV.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_INCENDGRENADE.Draw", { "weapons/tfa_csgo/flashbang/flashbang_draw.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_FRAGGRENADE.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_FRAGGRENADE.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_INCENDGRENADE.PullPin_Grenade_Start", { "weapons/tfa_csgo/flashbang/pinpull_start.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_INCENDGRENADE.PullPin_Grenade", { "weapons/tfa_csgo/flashbang/pinpull.wav" } )

TFA.AddWeaponSound( "TFA_CSGO_FLASHGRENADE.BOOM", { "weapons/tfa_csgo/flashbang/flashbang_explode2.wav", "weapons/tfa_csgo/flashbang/flashbang_explode1.wav" } )