sound.Add( {
	name = "csgo_knife.Deploy",
	channel = CHAN_STATIC,
	volume = 0.4,
	level = 65,
	sound = "csgo_knife/knife_deploy1.wav"
} )

sound.Add( {
	name = "csgo_knife.Hit",
	channel = CHAN_STATIC,
	volume = 1.0,
	level = 65,
	sound = { "csgo_knife/knife_hit1.wav", "csgo_knife/knife_hit2.wav", "csgo_knife/knife_hit3.wav", "csgo_knife/knife_hit4.wav" }
} )

sound.Add( {
	name = "csgo_knife.HitWall",
	channel = CHAN_STATIC,
	volume = 1.0,
	level = 65,
	sound = { "csgo_knife/knife_hit_01.wav", "csgo_knife/knife_hit_02.wav", "csgo_knife/knife_hit_03.wav", "csgo_knife/knife_hit_04.wav", "csgo_knife/knife_hit_05.wav" }
} )

sound.Add( {
	name = "csgo_knife.HitWall_old",
	channel = CHAN_STATIC,
	volume = 1.0,
	level = 65,
	sound = { "csgo_knife/knife_hitwall1.wav", "csgo_knife/knife_hitwall2.wav", "csgo_knife/knife_hitwall3.wav", "csgo_knife/knife_hitwall4.wav" }
} )

sound.Add( {
	name = "csgo_knife.Slash",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = {97, 105},
	level = 65,
	sound = { "csgo_knife/knife_slash1.wav", "csgo_knife/knife_slash2.wav" }
} )

sound.Add( {
	name = "csgo_knife.Slash_old",
	channel = CHAN_STATIC,
	volume = {0.5, 1.0},
	pitch = {97, 105},
	level = 65,
	sound = { "csgo_knife/knife_slash1_old.wav", "csgo_knife/knife_slash2_old.wav" }
} )

sound.Add( {
	name = "csgo_knife.Stab",
	channel = CHAN_STATIC,
	volume = 1.0,
	level = 65,
	sound = "csgo_knife/knife_stab.wav"
} )

sound.Add( {
	name = "csgo_knife.Backstab",
	channel = CHAN_STATIC,
	volume = 1.0,
	soundlevel = 65,
	sound = "csgo_knife/knife_stab.wav"
} )

-- Butterfly
sound.Add( {
	name = "csgo_ButterflyKnife.backstab01",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_backstab01.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.backstab02",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_backstab02.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.draw01",
	channel = CHAN_ITEM,
	volume = 0.6,
	soundlevel = 65,
	sound = "csgo_knife/bknife_draw01.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.draw02",
	channel = CHAN_ITEM,
	volume = 0.6,
	soundlevel = 65,
	sound = "csgo_knife/bknife_draw02.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.look01_a",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_look01_a.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.look01_b",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_look01_b.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.look02_a",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_look02_a.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.look02_b",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_look02_b.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.look03_a",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_look03_a.wav"
} )

sound.Add( {
	name = "csgo_ButterflyKnife.look03_b",
	channel = CHAN_ITEM,
	volume = 0.4,
	soundlevel = 65,
	sound = "csgo_knife/bknife_look03_b.wav"
} )

sound.Add( {
	name = "csgo_KnifeFalchion.inspect",
	channel = CHAN_STATIC,
	volume = 1,
	soundlevel = 65,
	sound = "csgo_knife/knife_falchion_inspect.wav"
} )

sound.Add( {
	name = "csgo_KnifeFalchion.draw",
	channel = CHAN_STATIC,
	volume = {0.4, 0.9},
	pitch = {97, 105},
	soundlevel = 65,
	sound = "csgo_knife/knife_falchion_draw.wav"
} )

sound.Add( {
	name = "csgo_KnifeFalchion.Catch",
	channel = CHAN_STATIC,
	volume = {0.3, 0.7},
	pitch = {97, 105},
	soundlevel = 65,
	sound = "csgo_knife/knife_falchion_catch.wav"
} )

sound.Add( {
	name = "csgo_KnifeFalchion.Idlev2",
	channel = CHAN_STATIC,
	volume = 1,
	soundlevel = 65,
	sound = "csgo_knife/knife_falchion_idle.wav"
} )

sound.Add( {
	name = "csgo_Weapon.WeaponMove1", 
	channel = CHAN_ITEM,
	volume = 0.15,
	soundlevel = 65,
	sound = "csgo_knife/movement1.wav"
} )

sound.Add( {
	name = "csgo_Weapon.WeaponMove3",
	channel = CHAN_ITEM,
	volume = 0.15,
	soundlevel = 65,
	sound = "csgo_knife/movement3.wav"
} )

sound.Add( {
	name = "csgo_Weapon.WeaponMove2",
	channel = CHAN_ITEM,
	volume = 0.15,
	soundlevel = 65,
	sound = "csgo_knife/movement2.wav"
} )

sound.Add( {
	name = "csgo_KnifePush.Attack1Heavy",
	channel = CHAN_STATIC,
	volume = {0.1, 0.2},
	pitch = {98, 105},
	level = 65,
	sound = { "csgo_knife/knife_push_attack1_heavy_01.wav", "csgo_knife/knife_push_attack1_heavy_02.wav", "csgo_knife/knife_push_attack1_heavy_03.wav", "csgo_knife/knife_push_attack1_heavy_04.wav" }
} )

sound.Add( {
	name = "csgo_KnifePush.LookAtStart",
	channel = CHAN_STATIC,
	volume = 0.2,
	level = 65,
	sound = { "csgo_knife/knife_push_lookat_start.wav" }
} )

sound.Add( {
	name = "csgo_KnifePush.LookAtEnd",
	channel = CHAN_STATIC,
	volume = 0.2,
	level = 65,
	sound = { "csgo_knife/knife_push_lookat_end.wav" }
} )

sound.Add( {
	name = "csgo_KnifePush.Draw",
	channel = CHAN_STATIC,
	volume = 0.3,
	level = 65,
	sound = { "csgo_knife/knife_push_draw.wav" }
} )

sound.Add( {
	name = "KnifeBowie.draw",
	channel = CHAN_STATIC,
	volume = {0.7, 0.8},
    pitch = {99, 100},
	level = 65,
	sound = { "csgo_knife/knife_bowie_draw.wav" }
} )

sound.Add( {
	name = "KnifeBowie.LookAtStart",
	channel = CHAN_STATIC,
	volume = {0.2, 0.2},
    pitch = {99, 100},
	level = 65,
	sound = { "csgo_knife/knife_bowie_inspect_start.wav" }
} )

sound.Add( {
	name = "KnifeBowie.LookAtEnd",
	channel = CHAN_STATIC,
	volume = {0.2, 0.3},
    pitch = {99, 101},
	level = 65,
	sound = { "csgo_knife/knife_bowie_inspect_end.wav" }
} )

--Gold Skins

if !TFA_CSGO_SKINS then
	TFA_CSGO_SKINS = {}
end

if !TFA_CSGO_SKINS["tfa_csgo_ctknife"] then
	TFA_CSGO_SKINS["tfa_csgo_ctknife"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_ctknife"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_default_ct/gold/knife_ct"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_tknife"] then
	TFA_CSGO_SKINS["tfa_csgo_tknife"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_tknife"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_default_t/gold/knife_t"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_bayonet"] then
	TFA_CSGO_SKINS["tfa_csgo_bayonet"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_bayonet"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_bayonet/gold/knife_bayonet"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_butfly"] then
	TFA_CSGO_SKINS["tfa_csgo_butfly"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_butfly"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_butterfly/gold/knife_butterfly"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_falch"] then
	TFA_CSGO_SKINS["tfa_csgo_falch"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_falch"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_falchion/gold/knife_falchion_advanced"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_flip"] then
	TFA_CSGO_SKINS["tfa_csgo_flip"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_flip"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_flip/gold/knife_flip"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_gut"] then
	TFA_CSGO_SKINS["tfa_csgo_gut"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_gut"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_gut/gold/knife_gut"			
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_karam"] then
	TFA_CSGO_SKINS["tfa_csgo_karam"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_karam"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_karambit/gold/karam"		
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_m9"] then
	TFA_CSGO_SKINS["tfa_csgo_m9"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_m9"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_m9/gold/knife_m9_bay"		
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_pushkn"] then
	TFA_CSGO_SKINS["tfa_csgo_pushkn"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_pushkn"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_push/gold/knife_push"		
	}
}

if !TFA_CSGO_SKINS["tfa_csgo_tackni"] then
	TFA_CSGO_SKINS["tfa_csgo_tackni"] = {}
end

TFA_CSGO_SKINS["tfa_csgo_tackni"].Gold =  {
	['name'] = "Gold",
	['tbl'] = {
		"models/tfa_csgo/knife_tactical/gold/knife_tactical"		
	}
}
